# TODO:
1. Documentation
2. less hard coded shit [x]
3. make generic and give it its own repo

# Configuration Variables
In the `_config.yml` the following custom variables need to be set:
```yaml
. . .
download_dir: downloads # default is downloads, this is for where the pdf will be located
file_name: resume.pdf # name of the pdf
stylesheet: ./assets/main.css # name of the stylesheet for the pdf  
. . .
```
For production, you will need to set the `baseurl` variable, and the `url` variable. set the `url` to your site's domain, ie: `https://example.com`
